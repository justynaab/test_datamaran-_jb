

const mongoose = require('mongoose');
// Db connection
mongoose.connect('mongodb://datamaran:datamaranjobfairtest@datamaran-dev-shard-00-00-hm29w.mongodb.net:27017,datamaran-dev-shard-00-01-hm29w.mongodb.net:27017,datamaran-dev-shard-00-02-hm29w.mongodb.net:27017/webservice?authSource=admin&replicaSet=datamaran-dev-shard-0&ssl=true', {useNewUrlParser: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() { // Create the server once the connection is open
    console.log('connection to db success');
    const http = require('http');
    const app = require('./app')
    const PORT = 5000; 
    const server = http.createServer(app);
    server.listen({
        port: PORT
    }, () => {
        console.log('Server is listening on port ' + PORT);
    });
});