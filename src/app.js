const express = require('express');
const app = express();
const bodyParser = require('body-parser'); 
const morgan = require('morgan'); 
const conso = require('consolidate')
const routes = require('./routes');

app.engine('html', conso.swig);
app.use(morgan('dev')); 
app.use(bodyParser.json());
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use('/css', express.static(__dirname + '/views/css'));
app.use('/img', express.static(__dirname + '/views/img'));
app.use('/js', express.static(__dirname + '/views/js'));
app.use('/',routes);


module.exports = app;
