const router = require('express').Router();
var mongoose = require('mongoose');
var schema = new mongoose.Schema({user:'string',features:'number',reliability:'number',durability:'number',serviceability:'number',aesthetics:'number'}, { collection: 'customer_data' });
var schema_employe_data = new mongoose.Schema({user:'string',features:'number',reliability:'number',durability:'number',serviceability:'number',aesthetics:'number'}, { collection: 'employe_data' });
var customerDataModel = mongoose.model('customer_data',schema);
var employeDataModel = mongoose.model('employe_data',schema_employe_data);
getData = ()=>{ // return an object  data = {custormerData:{},employeData:{}} custormerData contain all curstomer data  //  employeData contain all employe data
    var data = {customerData:{},employeData:{}}
    return new Promise((resolve, reject) => {
        customerDataModel.find({},function(err, customerData){
            if (err) throw err;
            else {
                data.customerData = customerData;
                employeDataModel.find({},function(err, employeData){
                    if (err) throw err;
                    else data.employeData = employeData; resolve(data)
                })
            }
        })
    });
};

router.get('/', function (req, res) {
    getData().then(data => { 
      res.send(data);
    });
  })
module.exports = {router};